import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import PeopleIcon from "@material-ui/icons/People";
import AssignmentIcon from "@material-ui/icons/Assignment";
import Button from "@material-ui/core/Button";
import AdjustIcon from "@material-ui/icons/Adjust";
import Tasks from "./components/Tasks";
import Goals from "./components/Goals";
import Users from "./components/Users";

export default function Routes() {
  return (
    <Router>
      <div>
        <nav>
          <ul
            style={{
              margin: "20px auto",
              display: "flex",
              justifyContent: "space-between",
              width: "28%",
            }}
          >
            <Link to="/">
              <Button
                color="primary"
                variant="contained"
                endIcon={<AssignmentIcon />}
              >
                Tasks
              </Button>
            </Link>
            <Link to="/goals">
              <Button
                color="primary"
                variant="contained"
                endIcon={<AdjustIcon />}
              >
                Goals
              </Button>
            </Link>
            <Link to="/users">
              <Button
                color="primary"
                variant="contained"
                endIcon={<PeopleIcon />}
              >
                Users
              </Button>
            </Link>
          </ul>
        </nav>

        <Switch>
          <Route path="/goals">
            <Goals />
          </Route>
          <Route path="/users">
            <Users />
          </Route>
          <Route path="/">
            <Tasks />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export const status = ["NEW", "WIP", "REVIEW", "DONE", "REOPEN"];
export const priority = [
  { label: "LOW", color: "green" },
  { label: "MEDIUM", color: "blue" },
  { label: "HIGH", color: "orange" },
  { label: "CRITICAL", color: "red" },
];

export const baseURL = "http://localhost:8000/api";

import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { baseURL, status, priority } from "../constants";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import axios from "axios";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

export default function BasicTable({ tasks, fetchTasks }) {
  const classes = useStyles();

  const editTask = (id) => {
    axios
      .patch(`${baseURL}/tasks/${id}`)
      .then((res) => console.log(res))
      .catch((error) => console.log(error));
  };

  const deleteTask = (id) => {
    axios
      .delete(`${baseURL}/tasks/${id}`)
      .then(fetchTasks)
      .catch((error) => console.log(error));
  };

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>ID</TableCell>
            <TableCell align="center">Name</TableCell>
            <TableCell align="center">Start Date</TableCell>
            <TableCell align="center">End Date</TableCell>
            <TableCell align="center">Status</TableCell>
            <TableCell align="center">Priority</TableCell>
            <TableCell align="center">Assignee</TableCell>
            <TableCell align="center">Goal</TableCell>
            <TableCell align="center">Dependant Tasks</TableCell>
            <TableCell align="center">Edit</TableCell>
            <TableCell align="center">Delete</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {tasks.map((row) => (
            <TableRow key={row.id}>
              <TableCell component="th" scope="row">
                {row.id}
              </TableCell>
              <TableCell align="center">{row.name}</TableCell>
              <TableCell align="center">{row.start_date}</TableCell>
              <TableCell align="center">{row.end_date}</TableCell>
              <TableCell align="center">{status[row.status]}</TableCell>
              <TableCell align="center">
                <span
                  style={{
                    background: priority[row.priority].color,
                    color: "white",
                    borderRadius: ".5em",
                    padding: 5,
                  }}
                >
                  {priority[row.priority].label}
                </span>
              </TableCell>
              <TableCell align="center">{row.assignee}</TableCell>
              <TableCell align="center">{row.goal}</TableCell>
              <TableCell align="center">
                [{row.dependant_task.join(", ")}]
              </TableCell>
              <TableCell align="center">
                <IconButton onClick={() => editTask(row.id)}>
                  <EditIcon />
                </IconButton>
              </TableCell>
              <TableCell align="center">
                <IconButton onClick={() => deleteTask(row.id)}>
                  <DeleteIcon />
                </IconButton>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

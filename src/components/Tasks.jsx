import React, { useEffect, useState } from "react";
import { Container, Paper } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from "@material-ui/icons/Add";
import axios from "axios";
import TaskTable from "./TaskTable";
import { baseURL } from "../constants";

const TaskContainer = (props) => {
  const [tasks, setTasks] = useState(null);
  useEffect(() => {
    fetchTasks();
  }, []);

  const fetchTasks = () => {
    axios(`${baseURL}/tasks`)
      .then((res) => setTasks(res.data))
      .catch((error) => console.log(error));
  };

  console.log(tasks);
  return (
    <Container>
      <Paper elevation={3}>
        <div style={{ display: "flex", justifyContent: "space-around" }}>
          <h2 style={{ flex: 1 }}>Tasks</h2>
          <IconButton>
            <AddIcon />
          </IconButton>
        </div>
        {tasks && <TaskTable tasks={tasks} fetchTasks={fetchTasks} />}
      </Paper>
    </Container>
  );
};

export default TaskContainer;
